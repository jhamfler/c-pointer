#include <stdio.h>
#include <stdlib.h>

// alloziere auf HEAP - !Stack
// Pointer auf Pointer == Pointer auf Array == Array auf Pointer == 2D Array
void f2 (int ** a, int size) {
	*a = malloc(sizeof(int) * size); // liefert Pointer auf reservierten Heapspeicher
	if(*a == NULL) printf("malloc schlug fehl\n");
	(*a)[0] = 11; // == **a == *pi
	*(*a+3) = 30; // == *a[3] == *(a+(4*3Bytes)) // für 32bit Architekturen
}

// pi wird nicht geändert, aber der Inhalt der Liste auf die pi zeigt
void f3 (int * a) {
	a[2] = 99;
}

// ohne Pointer auf Pointer gehts nicht, da Call by Value
void f4 (int * a, int size) {
	a=(int*)malloc(sizeof(int)*size);
	if(a == NULL) printf("malloc schlug fehl\n");
	a[0] = 44;
	a[3] = 66;
}

int main () {
	int * pi;

	f2(&pi, 10);
	printf("%d  \n", pi[0] );
	printf("%d  \n\n", pi[3] );

	f3(pi);
	printf("%d  \n\n", pi[2]);

	int * j;
	f4(j, 10);
	printf("%d  \n", *j);
}
