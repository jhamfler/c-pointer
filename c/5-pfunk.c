#include <stdio.h>
// parameter init äquivalent zu: int *a=&i
int f (int *a) {
	*a=1;
	return *a;
}
int main () {
	int i=0;

	printf("%d  \n", f(&i) );
	printf("%d  \n\n", i  );
}
