#include <stdio.h>
struct foo {
	size_t i;
	char c;
	int d [5];
};

int main () {
	void * pf;
	struct foo f;
	f.i = 11;
	f.c = 'a';
	f.d[0] = 0;
	f.d[2] = 3;
	pf=&f;
	printf("f.i = %d\tf.c = %c\tf.d[1] = %d\t*pf.d[1] = %d\t*pf.d[2] = %d\n",
          f.i,      f.c,      f.d[1],    (*(struct foo*)pf).d[1], ((struct foo*)pf)->d[2] );

	printf("pf = %x\n", pf);
	printf("*pf = %x &f = %x f = %x\n", *(struct foo*)pf, &(f), f);
	printf("&f.i = %x\n", &(f.i));

}
