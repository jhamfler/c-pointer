#include <stdio.h>
typedef struct list_entry {
    int val;
    struct list_entry *next;
} list_entry;

int main(){
	int i, ls=10;
	struct list_entry l[ls];

	for (i=0; i < ls; i++) {
		l[i].val  = i;
		l[i].next = &(l[i+1]);
	}
	l[ls-1].next = NULL;

	struct list_entry * head = &(l[0]);
	int to_remove = 3;



	// ab hier ist es relevant
	list_entry *entry = head; /* assuming head exists and is the first entry of the list */
	list_entry *prev = NULL;

	while (entry) {
	    if (entry->val == to_remove)     /* this is the one to remove */
	        if (prev)
	           prev->next = entry->next; /* remove the entry */
	        else
	            head = entry->next;      /* special case - first entry */

	    /* move on to the next entry */
	    prev = entry;
	    entry = entry->next;
	}
	// bis hier ist es relevant

	for (i=0; i < ls; i++)
		printf("%d\t%x\n", l[i].val, l[i].next);
}
